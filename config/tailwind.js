module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',
      black: '#000',
      white: '#fff',
      grey: {
        100: '#f5f5f5',
        200: '#dcdcdc',
        300: '#b4b4b4',
        400: '#999999',
        500: '#888888',
        default: '#888888',
        600: '#666666',
        700: '#4a4a4a',
        800: '#333333',
        900: '#222222'
      },
    },
    extend: {}
  },
  variants: {},
  plugins: []
}
